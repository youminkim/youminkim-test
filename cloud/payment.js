module.exports = function(){
	var config = require('cloud/config')
	var express = require('express');
	var app = express();

	// product static pricing
	// /product?amount=1.00&credit=1000
	app.get('/product', function(req, res) {
		var amount = req.query.amount;
		var credit = req.query.credit;
		var title = config.app_name + " Credit!";
		var title_plural = config.app_name + " Credits!";
		var image = config.base_url + config.logo_url;
		var desc = 'Buy ' + credit + ' credits.';
		var url = req.protocol + '://' + req.get('host') + req.originalUrl;
		res.render('product', 
			{ 
				title: title,
				title_plural: title_plural,
				desc: desc,
				url: url,
				image: image,
				amount: amount
			}
		);
	});

	// TODO: product dynamic pricing
	// https://developers.facebook.com/docs/howtos/payments/definingproducts/#pricing_dynamic


	return app;
}();

