module.exports = function(){
	var config = require('cloud/config')
	var express = require('express');
	var app = express();

	app.get('/login_manual', login_manual);
	app.post('/login_manual', login_manual);
	app.get('/login_manual_callback', login_manual_callback);
	app.post('/login_manual_callback', login_manual_callback);

	function login_manual(req, res){
		var url = "https://www.facebook.com/" + config.api_version + 
			"/dialog/oauth?client_id=" + config.app_id + "&redirect_uri=" + config.redirect_url + 
			"&scope=user_friends,email";
		res.redirect(url);
	}

	function login_manual_callback(req, res){
		var error = req.query.error;
		if (error){
			// Go to page or app center
			res.redirect(config.appcenter_url); // res.redirect(page_url);	
		} else {
			// Go to the game
			res.redirect(config.game_url);
		}
	}

	return app;
}();

