// These two lines are required to initialize Express in Cloud Code.
var express = require('express');
var app = express();

var config = require('cloud/config')


// Global app configuration section
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(express.bodyParser());    // Middleware for reading request body

function canvas(req, res){
	res.render('canvas', {});
}
function canvas_login(req, res){
	res.render('canvas_login', {});
}
function canvas_testapp(req, res){
	res.render('canvas_testapp', {});
}
function unity_redirect (req, res){
	res.redirect(config.base_url + config.unity_url);
}

app.get('/canvas', canvas);
app.post('/canvas', canvas);
app.get('/canvas_login', canvas_login);
app.post('/canvas_login', canvas_login);
app.get('/canvas_testapp', canvas_testapp);
app.post('/canvas_testapp', canvas_testapp);
app.get('/unity_redirect', unity_redirect);
app.post('/unity_redirect', unity_redirect);

app.use('/', require('cloud/payment'));
app.use('/', require('cloud/auth'));

// Attach the Express app to Cloud Code.
app.listen();
