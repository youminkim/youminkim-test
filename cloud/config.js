var config = {}

config.app_id = "1523333124610031";
config.app_name = "Youminkim Test";
config.api_version = "v2.3";
config.base_url = "https://youminkim-test.parseapp.com";
config.redirect_url = config.base_url + "/login_manual_callback";
config.page_url = "https://www.facebook.com/pages/Youminkim-Test-Community/805963249474569";
config.appcenter_url = "https://www.facebook.com/games/doubleucasino";
config.game_url = "https://apps.facebook.com/1523333124610031";
config.logo_url = "/img/logo.png";
config.unity_url = "/unity";


module.exports = config;
