require('cloud/app.js');
var config = require('cloud/config')

Parse.Cloud.define("events", function(request, response) {
	var send_events = function(body){
		Parse.Cloud.httpRequest({
			method: "POST",
			url: url,
			body: body,
			success: function(httpResponse) {
				console.log(httpResponse.text);
			},
			error: function(httpResponse) {
				console.error('0 Request failed with response code ' + httpResponse.status);
			}
		});
	}

	// TODO: test code
	var url = "https://graph.facebook.com/" + config.api_version + "/" + config.app_id + "/activities";
	var attr = "AEBE52E7-03EE-455A-B3C4-E57283966239";
	var now = Date.now() / 1000;
	var body = {
		id: app_id,
		event: "MOBILE_APP_INSTALL",
		attribution: attr,
		advertiser_tracking_enabled: 1,
		application_tracking_enabled: 1
	}
	send_events(body);

	custom_events = [{
		"_eventName": 'fb_mobile_purchase',
		"_valueToSum": 55.22,
		"_appVersion": "2.1.1",
		"_logTime": Math.floor(now),
		"fb_currency": 'USD'
	}];

	body = {
		id: app_id,
		event: "CUSTOM_APP_EVENTS",
		advertiser_id: attr,
		advertiser_tracking_enabled: 1,
		application_tracking_enabled: 1,
		custom_events : JSON.stringify(custom_events)
	}
	send_events(body);
});
